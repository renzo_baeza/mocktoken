﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MockToken.Models;

namespace MockToken.Controllers
{
    [RoutePrefix("apiToken/token")]
    public class TokenController : ApiController
    {
        // GET api/<controller>/5
        [HttpGet]
        [Route("getToken")]
        public string Get(int id)
        {
            return "value";
        }
        [HttpPost]
        [Route("validateToken")]
        public TokenModelo validateToken([FromBody] string token)
        {
            TokenModelo tokenModelo = new TokenModelo();
            tokenModelo.tokenStr = token;
            tokenModelo.userName = "RBaeza";
            return tokenModelo;
        }
    }
}